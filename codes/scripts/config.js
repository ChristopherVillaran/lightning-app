'use strict';

define('config', ['underscore'], (_) => {
    var mod = {
        'client': {
            'name': 'Collabspot',
            'api_url': 'https://wmphighrise.appspot.com/_ah/api'
        },
        'crms': {
            'Sugar': {
                'extension_id': ''
            },
            'Salesforce': {
                'extension_id': ''
            },
            'Lightning': {
                'extension_id': ''
            }
        },
        'user': {
            'email': 'christopher@collabspot.com'
        },
    };

    return mod;
});
