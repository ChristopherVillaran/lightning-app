'use strict';

define('comms', ['config', 'utility'], (Config, Utility) => {
    var client = Config.client;
    var crms = Config.crms;
    var cRuntime = chrome.runtime;
    var cTabs = chrome.tabs;
    var mod = {
        send: (receiver, title, data) => {
            var msg = {
                receiver,
                title,
                data
            };

            if (receiver in crms) {
                msg.sender = client.name.toLowerCase();
                Utility.getExtensionID(receiver, (id) => {
                    cRuntime.sendMessage(id, msg);
                });

            } else {
                msg.sender = 'background';
                cTabs.query({active: true}, (tabs) => {
                    cTabs.sendMessage(tabs[0].id, msg);
                });
            }
        },

        receive: (sender, callback) => {
            function incomingMessage(request, extension, sendResponse) {
                var msg = {
                    sender: request.sender,
                    receiver: request.receiver,
                    title: request.title,
                    data: request.data
                };

                if (sender === msg.sender) {
                    callback(msg);
                }
            }

            if (sender in crms) {
                cRuntime.onMessageExternal.addListener(incomingMessage);
            } else if (sender === 'lightning') {
                cRuntime.onMessageExternal.addListener(incomingMessage);
            } else {
                cRuntime.onMessage.addListener(incomingMessage);
            }
        }
    };

    return mod;
});
