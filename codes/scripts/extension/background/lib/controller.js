define('controller', ['comms', 'radio'], (Comms, Radio) => {
    var backgroundCh = Radio.channel('background');
    var contentCh = Radio.channel('content');
    var pageCh = Radio.channel('page');
    var sugarCh = Radio.channel('sugar');
    var mod = {
        backgroundCh,
        contentCh,
        pageCh,
        sugarCh
    };

    backgroundCh.reply({

    });

    contentCh.reply({

    });

    pageCh.reply({

    });

    sugarCh.reply({

    });

    return mod;
});
