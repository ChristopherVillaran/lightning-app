'use strict';

// Use JSON-like convention here in config
require.config({
    'baseUrl': chrome.extension.getURL('codes/scripts'),
    'paths': {
        'utility': 'utility',
        'config': 'config',

        // Background
        'comms': 'extension/background/lib/comms',
        'controller': 'extension/background/lib/controller',

        // Vendors
        'jquery': 'vendors/jquery',
        'underscore': 'vendors/underscore',
        'backbone': 'vendors/backbone/backbone',
        'radio': 'vendors/backbone/radio',
        'forcetk': 'vendors/forcetk'
    },
    'shim': {
        'radio': ['backbone','underscore'],
        'forcetk': ['jquery']
    }
});
