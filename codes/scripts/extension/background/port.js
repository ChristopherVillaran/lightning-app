'use strict';

require(['comms', 'controller'], (Comms, Controller) => {
    Comms.receive('content', (msg) => {
        Controller.contentCh.request(msg.title, msg.data);
    });

    Comms.receive('page', (msg) => {
        Controller.pageCh.request(msg.title, msg.data);
    });
});
