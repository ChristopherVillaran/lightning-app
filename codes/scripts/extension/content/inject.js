'use strict';

require(['utility'], (utility) => {
    utility.injectScript('vendorScript','body','require','append');
    utility.injectScript('pageScript','body','organizer','append');

    function setUpPageScripts() {
        window.addEventListener('message', (event) => {
            var evData = event.data;
            if (evData.sender && evData.sender === 'page/organizer') {

                // if page-organizer is injected, pass extension id to page-organizer.js
                if (evData.msg && evData.msg === 'injected') {
                    window.postMessage({
                        sender: 'inject',
                        msg: chrome.extension.getURL('codes/scripts')
                    }, window.location.href);

                // if page-organizer is ready, inject page.js
                } else if (evData.msg && evData.msg == 'ready') {
                    utility.injectScript('pageScript','body','port','append');
                    utility.injectScript('pageScript','body','page','append');
                }
            }
        }, false);
    }

    function setUpDomElements() {
        var sidebarContainer = '<div id="collabspot-sidebar"></div>';

        $('body')
            .append(sidebarContainer)
            .children('#collabspot-sidebar')
                .append();
    }

    // setUpDomElements();
    setUpPageScripts();
});
