'use strict';

define('comms', ['config'], (Config) => {
    var cRuntime = chrome.runtime;
    var crms = Config.crms;
    var mod = {
        send: (receiver, title, data, sender) => {
            var msg = {
                sender: sender || 'content',
                receiver,
                title,
                data
            };

            switch (receiver) {
                case 'background':
                    cRuntime.sendMessage(msg);
                    break;

                case 'page':
                    window.postMessage(msg, window.location.href);
                    break;

                default:
                    if (receiver in crms) {
                        msg.sender = 'collabspot';
                    }
            }
        },

        receive: (sender, callback) => {
            function incomingMessage(request, extension, sendResponse) {
                var msg = {
                    sender: request.sender,
                    receiver: request.receiver,
                    title: request.title,
                    data: request.data
                };

                if (sender === msg.sender) {
                    callback(msg);
                }
            }

            switch (sender) {
                case 'background':
                    cRuntime.onMessage.addListener(incomingMessage);
                    break;

                case 'page':
                    window.addEventListener('message', (event) => {
                        var evData = event.data;
                        if (evData.sender && evData.sender === 'page') {
                            let msg = {
                                sender: evData.sender,
                                receiver: evData.receiver,
                                title: evData.title,
                                data: evData.data
                            };
                            callback(msg);
                        }
                    }, false);
            }
        }
    };

    return mod;
});
