define('controller', ['comms', 'radio'], (Comms, Radio) => {
    var backgroundCh = Radio.channel('background');
    var contentCh = Radio.channel('content');
    var pageCh = Radio.channel('page');
    var mod = {
        backgroundCh,
        contentCh,
        pageCh
    };

    backgroundCh.reply({
        
    });

    contentCh.reply({

    });

    pageCh.reply({

    });

    return mod;
});
