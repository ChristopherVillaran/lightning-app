'use strict';

require(['jquery'], ($) => {
    let path = 'https://ap2.lightning.force.com/c/HelloWorldApp.app';

    $('<iframe></iframe>',{
        'src': path,
        'id': 'cs-sf-lightning-component'
    })
        .appendTo('body')
        .css({
            'width': '200px',
            'height': '200px',
            'z-index': '999',
            'border': 'none',
            'position': 'absolute',
            'top': '20px',
            'left': '200px'
        });

    window.addEventListener('message', (event) => {
        var evData = event.data;
        if (evData.sender && evData.sender === 'lightning') {
            console.log(evData);
        }
    }, false);

    chrome.runtime.onMessage.addListener(function (request, extension, sendResponse) {
        console.log(request);
    });
});
