'use strict';

require(['comms', 'controller'], (Comms, Controller) => {
    Comms.receive('page', (msg) => {
        if (msg.receiver === 'content') {
            Controller.pageCh.request(msg.title, msg.data);

        // page -> background
        } else if (msg.receiver === 'background') {
            Comms.send(msg.receiver, msg.title, msg.data, msg.sender);
        }
    });

    Comms.receive('background', (msg) => {
        if (msg.receiver === 'content') {
            Controller.pageCh.request(msg.title, msg.data);

        // page <- background
        } else if (msg.receiver === 'page') {
            Comms.send(msg.receiver, msg.title, msg.data, msg.sender);
        }
    });
});
