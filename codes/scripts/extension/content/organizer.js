'use strict';

// Use JSON-like convention here in config
require.config({
    'baseUrl': chrome.extension.getURL('codes/scripts'),
    'paths': {
        'utility': 'utility',
        'config': 'config',

        // Content
        'comms': 'extension/content/lib/comms',
        'controller': 'extension/content/lib/controller',

        // Vendors
        'backbone': 'vendors/backbone/backbone',
        'babysitter': 'vendors/backbone/babysitter',
        'jquery': 'vendors/jquery',
        'marionette': 'vendors/backbone/marionette',
        'underscore': 'vendors/underscore',
        'radio': 'vendors/backbone/radio'
    },
    'shim': {
        'marionette': ['backbone','babysitter','radio'],
        'babysitter': ['backbone','underscore'],
        'radio': ['backbone','underscore'],
        'backbone': ['jquery','underscore'],
    }
});
