'use strict';

require(['comms', 'controller'], (Comms, Controller) => {
    Comms.receive('collabspot/background', (msg) => {
        Controller.backgroundCh.request(msg.title, msg.data);
    });

    Comms.receive('collabspot/content', (msg) => {
        Controller.contentCh.request(msg.title, msg.data);
    });
});
