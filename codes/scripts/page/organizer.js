'use strict';

(() => {
    function setRequireConfig(url) {

        // Use JSON-like convention here in config
        require.config({
            'baseUrl': url,
            'paths': {
                'utility': 'utility',

                // Vendors
                'jquery': 'vendors/jquery',
                'underscore': 'vendors/underscore',
                'gmail': 'vendors/gmail',
                'backbone': 'vendors/backbone/backbone',
                'radio': 'vendors/backbone/radio',

                // Page
                'comms': 'page/lib/comms',
                'controller': 'page/lib/controller'
            },
            'shim': {
                'radio': ['backbone','underscore'],
                'gmail': {
                    'deps': ['jquery'],
                    'init': () => {
                        return Gmail();
                    }
                }
            }
        });
    }

    // tell inject.js that page-organizer.js is injected
    window.postMessage(
        {
            sender: 'page/organizer',
            msg: 'injected'
        },
        window.location.href
    );

    window.addEventListener('message', (event) => {
        var evData = event.data;
        if (evData.sender && evData.sender === 'inject') {
            setRequireConfig(evData.msg);

            // tell inject.js that page-organizer.js is ready
            window.postMessage(
                {
                    sender: 'page/organizer',
                    msg: 'ready'
                },
                window.location.href
            );
        }
    }, false);
}());
