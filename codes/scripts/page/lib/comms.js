'use strict';

define('comms', () => {
    var mod = {
        send: (receiver, title, data) => {
            var msg = {
                sender: 'page',
                receiver,
                title,
                data
            };
            window.postMessage(msg, window.location.href);
        },

        receive: (sender, callback) => {
            window.addEventListener('message', (event) => {
                var evData = event.data;
                
                if (evData.sender && evData.sender === sender) {
                    let msg = {
                        sender: evData.sender,
                        receiver: evData.receiver,
                        title: evData.title,
                        data: evData.data
                    };
                    callback(msg);
                }
            }, false);
        }
    };

    return mod;
});
