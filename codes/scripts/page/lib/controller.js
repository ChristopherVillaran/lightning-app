define('controller', ['comms', 'radio'], (Comms, Radio) => {
    var backgroundCh = Radio.channel('background');
    var contentCh = Radio.channel('content');
    var pageCh = Radio.channel('page');
    var mod = {
        backgroundCh,
        contentCh,
        pageCh
    };

    backgroundCh.reply({
        'initialized': () => {
            console.log('background to page');
        }
    });

    contentCh.reply({
        'initialized': () => {
            console.log('content to page');
        }
    });

    pageCh.reply({
        'initialize': () => {
            Comms.send('background','initialized');
            Comms.send('content','initialized');
        }
    });

    return mod;
});
