'use strict';

define('utility', ['config', 'jquery', 'underscore'], (Config, $, _) => {
    var paths = {
        template: 'codes/templates/',
        vendorScript: 'codes/scripts/vendors/',
        contentScript: 'codes/scripts/extension/content/',
        backgroundScript: 'codes/scripts/extension/background/',
        pageScript: 'codes/scripts/page/',
        script: 'codes/scripts/'
    };

    var mod = {
        clone: (obj) => {
            if (obj) {
                return JSON.parse(JSON.stringify(obj));
            } else if (obj === null) {
                return null;
            } else if (typeof obj === undefined) {
                return undefined;
            }
        },

        curry: function(fn) {
            // var args = arguments.slice(1);
            // return fn.apply(this, );
        },

        firstLetterToUpperCase: (word) => {
            return word.charAt(0).toUpperCase() + word.substr(1);
        },

        injectTemplate: (target, file, method) => {
            var path = `${paths.template}${file}.html`;

            $.ajax({
                url: chrome.extension.getURL(path)
            }).then((response) => {
                $(target)[method](response);
            });
        },

        injectScript: (type, target, file, method) => {
            var path;
            if (type === 'cdn') {
                path = file;
            } else {
                path = `${chrome.extension.getURL(paths[type] + file)}.js`;
            }

            var scriptTag = `<script src='${path}'></script>`;
            $(target)[method](scriptTag);
        },

        getExtensionID: function(crm, callback) {
            var crms = Config.crms;
            var _crm = mod.firstLetterToUpperCase(crm);

            if (crms[_crm].extension_id) {
                callback(crms[_crm].extension_id);

            } else {
                chrome.management.getAll((apps) => {
                    var name;
                    _.each(apps, (app) => {
                        name = app.name;
                        if (crm === name.toLowerCase()) {
                            crms[name].extension_id = app.id;
                            callback(crms[name].extension_id);
                        }
                    });
                });
            }
        }
    };

    return mod;
});
